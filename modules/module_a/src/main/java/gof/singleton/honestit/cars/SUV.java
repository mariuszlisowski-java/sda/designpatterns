package gof.singleton.honestit.cars;

public class SUV extends Car {

    public SUV() {
        this.setType("SUV");
    }
}
