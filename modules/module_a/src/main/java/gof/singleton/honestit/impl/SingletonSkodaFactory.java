package gof.singleton.honestit.impl;

import gof.singleton.honestit.factories.SkodaFactory;

public class SingletonSkodaFactory extends SkodaFactory {

    public static SingletonSkodaFactory instance() {
        return InstanceWrapper.instance;
    }

    private SingletonSkodaFactory() {
        super();
    }

    private static class InstanceWrapper {
        public static final SingletonSkodaFactory instance = new SingletonSkodaFactory();
    }
}
