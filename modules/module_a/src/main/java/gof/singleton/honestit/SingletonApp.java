package gof.singleton.honestit;

import gof.singleton.honestit.cars.Car;
import gof.singleton.honestit.cars.Combi;
import gof.singleton.honestit.cars.SUV;
import gof.singleton.honestit.cars.Sedan;
import gof.singleton.honestit.factories.CarFactory;
import gof.singleton.honestit.factories.FiatFactory;
import gof.singleton.honestit.factories.SkodaFactory;
import gof.singleton.honestit.impl.SingletonCarFactory;
import gof.singleton.honestit.impl.SingletonFiatFactory;

public class SingletonApp {

    public static void main(String[] args) {
        // slow instantiation
        System.out.println("Instantiation using regular factories...");

        Car car1 = new Combi();
        car1.setKM("180 KM");
        car1.setVolume("1.8");
        car1.setMade("Skoda");
        car1.setPrice("750000,00");
        car1.setName("Octavia III Combi");

        Car car2 = new SUV();
        car2.setKM("210 KM");
        car2.setVolume("2.0");
        car2.setMade("Skoda");
        car2.setPrice("1100000,00");
        car2.setName("Karoq");

        Car car3 = new Sedan();
        car3.setKM("150 KM");
        car3.setVolume("1.5");
        car3.setMade("Skoda");
        car3.setPrice("700000,00");
        car3.setName("Octavia III");

        SkodaFactory skodaFactory = new SkodaFactory();
        Car car4 = skodaFactory.combi();
        Car car5 = skodaFactory.suv();
        Car car6 = skodaFactory.sedan();

        Car car7 = CarFactory.getFactory(CarFactory.Made.SKODA).combi();
        Car car8 = CarFactory.getFactory(CarFactory.Made.SKODA).suv();
        Car car9 = CarFactory.getFactory(CarFactory.Made.SKODA).sedan();

        Car car10 = CarFactory.getDefaultFactory().combi();
        Car car11 = CarFactory.getDefaultFactory().suv();
        Car car12 = CarFactory.getDefaultFactory().sedan();

        /*
            singleton
         */
        // much faster instantiation
        System.out.println("Instantiation using singleton factories...");

        FiatFactory fiatFactory = SingletonFiatFactory.instance();
        Car fiatCombi = fiatFactory.combi();
        Car fiatSuv = fiatFactory.suv();
        Car fiatSedan = fiatFactory.sedan();

        // reuse the same factory
        Car skodaCombi = SingletonCarFactory.getFactory(CarFactory.Made.SKODA).combi();
        Car skodaSuv = SingletonCarFactory.getFactory(CarFactory.Made.SKODA).suv();
        Car skodaSedan = SingletonCarFactory.getFactory(CarFactory.Made.SKODA).sedan();

        // reuse the same factory
        Car defaultCombi = SingletonCarFactory.getDefaultFactory().combi();
        Car defaultSuv = SingletonCarFactory.getDefaultFactory().suv();
        Car defaultSedan = SingletonCarFactory.getDefaultFactory().sedan();
    }
}
