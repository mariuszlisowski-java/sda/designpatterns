package gof.singleton.honestit.cars;

public class Sedan extends Car {

    public Sedan() {
        this.setType("Sedan");
    }
}
