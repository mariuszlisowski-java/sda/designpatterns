package gof.singleton.types.eager.classes;

public class Main {

    public static void main(String[] args) {
        SimpleCounter counterA = SimpleCounter.getInstance();
        SimpleCounter counterB = SimpleCounter.getInstance();

        counterA.incrementCounter(); // now 1
        counterB.incrementCounter(); // now 2
        System.out.println(counterB.getCounter());

        // enable assertions (-ea in VM options)
        assert counterA == counterB; // references are the same
    }

}
