package gof.singleton.types.eager.classes;

public class SimpleCounter {
    // eager instance (initialized immediately)
    private static final SimpleCounter INSTANCE = new SimpleCounter();

    // hidden constructor
    private SimpleCounter() {}

    // static method returning singleton
    public static SimpleCounter getInstance() {
        return INSTANCE;
    }

    // other logic
    private int counter = 0;

    public void incrementCounter() {
        counter++;
    }

    public int getCounter() {
        return counter;
    }

}
