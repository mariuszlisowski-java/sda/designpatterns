package gof.singleton.types.lazy.regular;

public class Main {

    public static void main(String[] args) {
        CommonStorage storageA = CommonStorage.getInstance();
        CommonStorage storageB = CommonStorage.getInstance();
        CommonStorage storageC = CommonStorage.getInstance();

        // multiple instances
        storageA.addValue(5);
        storageB.addValue(7);
        storageC.addValue(9);

        // one storage (here: using storageC)
        for (Integer i : storageC.getValues()) {
            System.out.println(i);
        }
    }

}
