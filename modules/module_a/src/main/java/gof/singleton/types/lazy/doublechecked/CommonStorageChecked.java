package gof.singleton.types.lazy.doublechecked;

import java.util.ArrayList;
import java.util.List;

// safe for multithreading
public class CommonStorageChecked {
    // lazy instance (not initialized yet)
    private static CommonStorageChecked instance;

    // hidden c'tor
    private CommonStorageChecked() {}

    public static CommonStorageChecked getInstance() {
        // do not use synchronized if singleton exists
        if (instance == null) {
            // use costly synchronized block to instantiate singleteon
            synchronized (CommonStorageChecked.class) {
                // double checked if instance exists
                if (instance == null) {
                    instance = new CommonStorageChecked();
                }
            }
        }

        return instance;
    }

    // other logic
    private List<Integer> values = new ArrayList<>();

    public void addValue(Integer value) {
        values.add(value);
    }

    public List<Integer> getValues() {
        return values;
    }
}
