package gof.singleton.types.eager.enums;

public enum SimpleCounter {
    // enum initialized once on JVM start (eager)
    INSTANCE;

    // other logic
    private int counter = 0;

    public void incrementCounter() {
        counter++;
    }

    public int getCounter() {
        return counter;
    }
}
