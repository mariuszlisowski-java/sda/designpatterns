package gof.singleton.types.lazy.regular;

import java.util.ArrayList;
import java.util.List;

// unsafe for multithreading
public class CommonStorage {
    // lazy instance (not initialized yet)
    private static CommonStorage instance;

    // hidden c'tor
    private CommonStorage() {}

    public static CommonStorage getInstance() {
        // not synchronized
        // can create additional instances in multithreading
        if (instance == null) {
            instance = new CommonStorage();
        }
        return instance;
    }

    // other logic
    private List<Integer> values = new ArrayList<>();

    public void addValue(Integer value) {
        values.add(value);
    }

    public List<Integer> getValues() {
        return values;
    }
}
