package gof.decorator.honestit.calculator;

import java.util.LinkedHashMap;
import java.util.Map;

public class CachedCalculator implements Calculator {
    private static final int CACHE_SIZE = 10;

    private Map<Integer, Long> factorialCache;
    private Map<Integer, Long> fibbonacciCache;

    private SimpleCalculator decorated;

    public CachedCalculator(SimpleCalculator decorated) {
        this.decorated = decorated;
        this.factorialCache = new LinkedHashMap<>(CACHE_SIZE) {
            protected boolean removeEldestEntry(Map.Entry<Integer, Long> eldest) {
                System.out.print("(checkng size of factorial cache)");
                return size() > CACHE_SIZE;
            }

        };
        this.fibbonacciCache = new LinkedHashMap<>(CACHE_SIZE) {
            protected boolean removeEldestEntry(Map.Entry<Integer, Long> eldest) {
                System.out.print("(checkng size of fibbonacci cache)");
                return size() > CACHE_SIZE;
            }
        };
    }

    @Override
    public long factorial(int n) {
        if (factorialCache.containsKey(n)) {
            System.out.print("[LOG] Factorial of " + n + " : cache used ");
            return factorialCache.get(n);
        }
        System.out.print("[LOG] Factorial of " + n + " : computed ");
        long result = decorated.factorial(n);
        factorialCache.put(n, result);
        System.out.print(" size of " + factorialCache.size() + " : ");

        return result;
    }

    @Override
    public long fibbonacci(int n) {
        if (fibbonacciCache.containsKey(n)) {
            System.out.print("[LOG] Fibbonacci of " + n + " : cache used ");
            return fibbonacciCache.get(n);
        }
        System.out.print("[LOG] Fibbonacci of " + n + " : computed ");
        long result = decorated.fibbonacci(n);
        fibbonacciCache.put(n, result);
        System.out.print(" size of " + fibbonacciCache.size() + " : ");

        return result;
    }

    public void clearCache() {
        factorialCache.clear();
        fibbonacciCache.clear();
        System.out.println("[LOG] Cache cleared!");
    }
}
