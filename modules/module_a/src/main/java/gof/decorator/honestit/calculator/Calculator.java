package gof.decorator.honestit.calculator;

public interface Calculator {
    long factorial(int n);
    long fibbonacci(int n);
    void clearCache();
}
