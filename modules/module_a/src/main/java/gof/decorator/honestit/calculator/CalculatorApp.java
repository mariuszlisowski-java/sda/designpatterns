package gof.decorator.honestit.calculator;

public class CalculatorApp {

    public static void main(String[] args) {
        Calculator simpleCalculator = new SimpleCalculator();
//        System.out.println(simpleCalculator.factorial(5));
//        System.out.println(simpleCalculator.fibbonacci(10));

        Calculator cachedCalculator = new CachedCalculator(new SimpleCalculator());
        // testing
        System.out.println(cachedCalculator.factorial(5));
        System.out.println(cachedCalculator.fibbonacci(10));
        // same again (to use cache)
        System.out.println(cachedCalculator.factorial(5));
        System.out.println(cachedCalculator.fibbonacci(10));

        System.out.println(cachedCalculator.fibbonacci(10));
        System.out.println(cachedCalculator.fibbonacci(11));
        System.out.println(cachedCalculator.fibbonacci(12));
        System.out.println(cachedCalculator.fibbonacci(13));
        System.out.println(cachedCalculator.fibbonacci(14));
        System.out.println(cachedCalculator.fibbonacci(15));
        System.out.println(cachedCalculator.fibbonacci(16));
        System.out.println(cachedCalculator.fibbonacci(17));
        System.out.println(cachedCalculator.fibbonacci(18));
        System.out.println(cachedCalculator.fibbonacci(19));
        System.out.println(cachedCalculator.fibbonacci(20));

        System.out.println(cachedCalculator.fibbonacci(10));
        System.out.println(cachedCalculator.fibbonacci(10));

        cachedCalculator.clearCache();
        System.out.println(cachedCalculator.fibbonacci(10));
    }

}
