package gof.decorator.honestit.bank;

import gof.decorator.honestit.bank.impl.AuditableBankAccount;
import gof.decorator.honestit.bank.impl.LoggableBankAccount;

import java.math.BigDecimal;

public class DecoratorApp {

    public static void main(String[] args) {
        BankAccount bankAccountA = new InMemoryBankAccount();
        bankAccountA.open("Ian", "Nobody", "12345");
        bankAccountA.deposit(BigDecimal.valueOf(20_000));

        BankAccount bankAccountB = new InMemoryBankAccount();
        bankAccountB.open("Tracy", "Smith", "54321");
        bankAccountB.deposit(BigDecimal.valueOf(20_000));

        bankAccountA.transfer(bankAccountB, BigDecimal.valueOf(10_000));
        bankAccountB.transfer(bankAccountA, BigDecimal.valueOf(5_000));

        bankAccountA.close();
        bankAccountB.close();

        /*
            decorator
         */
        // loggable
        BankAccount decoratedBankAccountA = new LoggableBankAccount(new InMemoryBankAccount());
        decoratedBankAccountA.open("Ian", "Nobody", "12345");
        decoratedBankAccountA.deposit(BigDecimal.valueOf(20_000));

        BankAccount decoratedBankAccountB = new LoggableBankAccount(new InMemoryBankAccount());
        decoratedBankAccountB.open("Tracy", "Smith", "54321");
        decoratedBankAccountB.deposit(BigDecimal.valueOf(20_000));

        decoratedBankAccountA.transfer(decoratedBankAccountB, BigDecimal.valueOf(10_000));
        decoratedBankAccountB.transfer(decoratedBankAccountA, BigDecimal.valueOf(5_000));

        decoratedBankAccountA.close();
//        decoratedBankAccountB.close();

        // auditable
        BankAccount auditableAccount = AuditableBankAccount.asAuditable(
                                            new LoggableBankAccount(new InMemoryBankAccount())
                                       );
        auditableAccount.open("Kim", "Basinger", "98765");
        auditableAccount.deposit(BigDecimal.valueOf(5_000));
        auditableAccount.withdraw(BigDecimal.valueOf(2_000));
        auditableAccount.transfer(decoratedBankAccountB, BigDecimal.valueOf(1_000));
        auditableAccount.close();
        // history check
        for (String record : ((AuditableBankAccount) auditableAccount).getHistory() ) {
            System.out.println(record);
        }
    }
}