package gof.decorator.honestit.bank.impl;

import gof.decorator.honestit.bank.BankAccount;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class AuditableBankAccount implements BankAccount {

    public static BankAccount asAuditable(BankAccount toAudit) {
        return new AuditableBankAccount(toAudit);
    }

    private BankAccount decorated;

    List<String> history;

    private AuditableBankAccount(BankAccount bankAccount) {
        this.decorated = bankAccount;
        history = new LinkedList<>();
    }

    @Override
    public String getIBAN() {
        history.add("[AUDITABLE] IBAN assigned");
        return decorated.getIBAN();
    }

    @Override
    public void open(String firstName, String lastName, String pesel) {
        history.add("[AUDITABLE] Account open");
        decorated.open(firstName, lastName, pesel);
    }

    @Override
    public BigDecimal close() {
        history.add("[AUDITABLE] Account closed");
        BigDecimal amount = decorated.close();
        return amount;
    }

    @Override
    public BigDecimal withdraw(BigDecimal amount) {
        history.add("[AUDITABLE] Money withdrawn");
        BigDecimal money = decorated.withdraw(amount);
        return money;
    }

    @Override
    public BigDecimal deposit(BigDecimal amount) {
        history.add("[AUDITABLE] Money deposited");
        BigDecimal money = decorated.deposit(amount);
        return money;
    }

    @Override
    public void transfer(BankAccount destination, BigDecimal amount) {
        history.add("[AUDITABLE] Money transferred");
        decorated.transfer(destination, amount);
    }

    public List<String> getHistory() {
        return Collections.unmodifiableList(history);
    }
}
