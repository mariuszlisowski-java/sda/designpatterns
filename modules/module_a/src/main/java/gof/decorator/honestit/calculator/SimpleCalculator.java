package gof.decorator.honestit.calculator;

public class SimpleCalculator implements Calculator {
    @Override
    public long factorial(int n) {
        return  n <= 1 ? 1 : n * factorial(n - 1);
    }

    @Override
    public long fibbonacci(int n) {
        return n <= 2 ? 1 : fibbonacci(n - 1) + fibbonacci(n - 2);
    }

    public void clearCache() {}
}
