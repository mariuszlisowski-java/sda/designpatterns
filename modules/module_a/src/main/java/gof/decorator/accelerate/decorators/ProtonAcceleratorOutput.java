package gof.decorator.accelerate.decorators;

public class ProtonAcceleratorOutput implements Accelerator {
    private final Accelerator decorated;

    public ProtonAcceleratorOutput(Accelerator accelerator) {
        this.decorated = accelerator;
    }

    @Override
    public void accelerate() {
        decorated.accelerate();
        System.out.println("Proton accelerated... current speed: " + getSpeed());
    }

    @Override
    public void reset() {
        decorated.reset();
        System.out.println("Proton stopped!");
    }

    @Override
    public int getSpeed() {
        return decorated.getSpeed();
    }
}
