package gof.decorator.accelerate.decorators;

public class ProtonAcceleratorLogger implements Accelerator {
    private final Accelerator decorated;

    public ProtonAcceleratorLogger(Accelerator decorated) {
        this.decorated = decorated;
    }


    @Override
    public void accelerate() {
        decorated.accelerate();
        System.out.println("[LOG] Proton accelerated... current speed: " + getSpeed());
    }

    @Override
    public void reset() {
        decorated.reset();
        System.out.println("[LOG] Proton stopped!");
    }

    @Override
    public int getSpeed() {
        return decorated.getSpeed();
    }
}
