package gof.decorator.accelerate.decorators;

import gof.decorator.accelerate.decorators.Accelerator;

public class ProtonAccelerator implements Accelerator {
    private int speed = 0;

    @Override
    public void accelerate() {
        speed++;
    }

    @Override
    public void reset() {
        speed = 0;
    }

    public int getSpeed() {
        return speed;
    }
}
