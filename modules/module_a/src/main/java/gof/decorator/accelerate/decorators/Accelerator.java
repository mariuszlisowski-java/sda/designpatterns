package gof.decorator.accelerate.decorators;

public interface Accelerator {
    void accelerate();
    void reset();
    int getSpeed();
}
