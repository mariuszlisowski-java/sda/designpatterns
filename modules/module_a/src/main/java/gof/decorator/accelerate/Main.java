package gof.decorator.accelerate;

import gof.decorator.accelerate.decorators.Accelerator;
import gof.decorator.accelerate.decorators.ProtonAccelerator;
import gof.decorator.accelerate.decorators.ProtonAcceleratorLogger;
import gof.decorator.accelerate.decorators.ProtonAcceleratorOutput;

public class Main {

    public static void main(String[] args) {
        ProtonAccelerator proton = new ProtonAccelerator();
        // no output
        proton.accelerate();
        proton.reset();

        /* decorators */
        Accelerator protonOutput = new ProtonAcceleratorOutput(proton);
        // console output
        protonOutput.accelerate();
        protonOutput.reset();

        Accelerator protonLogger = new ProtonAcceleratorLogger(proton);
        // logging
        protonLogger.accelerate();
        protonLogger.reset();
    }

}
