package gof.decorator.car;

import gof.decorator.car.decorators.CarPremium;

public class Main {

    public static void main(String[] args) {
        Car i20 = new i20Car();
        System.out.println(i20.description());

        Car i20Premium = new CarPremium(i20);
        System.out.println(i20Premium.description());
    }

}
