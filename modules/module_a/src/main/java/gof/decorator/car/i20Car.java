package gof.decorator.car;

import java.math.BigDecimal;

public class i20Car extends Car {
    @Override
    public BigDecimal price() {
        return BigDecimal.valueOf(20_000);
    }

    @Override
    public String description() {
        return "Hunday i20";
    }
}
