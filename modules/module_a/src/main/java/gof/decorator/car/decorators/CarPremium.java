package gof.decorator.car.decorators;

import gof.decorator.car.Car;

import java.math.BigDecimal;

public class CarPremium extends Car {
    Car decorated;

    public CarPremium(Car decorated) {
        this.decorated = decorated;
    }

    @Override
    public BigDecimal price() {
        return decorated.price().add(BigDecimal.valueOf(5_000));
    }

    @Override
    public String description() {
        return "Hunday i20 Premium";
    }
}
