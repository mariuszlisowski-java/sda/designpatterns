package gof.decorator.car;

import java.math.BigDecimal;

public abstract class Car {
    public abstract BigDecimal price();
    public abstract String description();
}
