package gof.decorator.car;

import java.math.BigDecimal;

public class i30Car extends Car {
    @Override
    public BigDecimal price() {
        return BigDecimal.valueOf(30_000);
    }

    @Override
    public String description() {
        return "Hunday i30";
    }
}
