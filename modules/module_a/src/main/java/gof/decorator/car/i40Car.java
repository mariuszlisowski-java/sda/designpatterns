package gof.decorator.car;

import java.math.BigDecimal;

public class i40Car extends Car {
    @Override
    public BigDecimal price() {
        return BigDecimal.valueOf(40_000);
    }

    @Override
    public String description() {
        return "Hunday i40";
    }
}
