package gof.observer.channel;

import gof.observer.channel.observers.AdminObserver;
import gof.observer.channel.observers.BaseObserver;
import gof.observer.channel.observers.UserObserver;

public class ObserverUsage {

    public static void main(String[] args) {
        final ChatChannel chatChannel = new ChatChannel("design-patterns");

        final BaseObserver userA = new UserObserver(chatChannel, "andrzej");
        final BaseObserver userB = new UserObserver(chatChannel, "ala");
        final BaseObserver adminA = new AdminObserver(chatChannel, "janusz");
        final BaseObserver adminB = new AdminObserver(chatChannel, "ania");

        userA.sendMessage("Hello all");
        userB.sendMessage("Hi Andrzej");
        adminA.sendMessage("ania they can't see what we write");
        adminB.sendMessage("Yes I know");
    }

}
