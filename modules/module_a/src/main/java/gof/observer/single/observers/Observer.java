package gof.observer.single.observers;

public interface Observer {
    void update();
}
