package gof.observer.single;

import gof.observer.single.observers.Observer;

public class Subject {

    public Subject(Observer observer) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 5; i++) {
                    System.out.println(i);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                // norify observer
                observer.update();
            }
        });
        thread.start();
    }
}
