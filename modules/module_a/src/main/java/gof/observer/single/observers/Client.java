package gof.observer.single.observers;

public class Client implements Observer {
    @Override
    public void update() {
        System.out.println("Work finished");
    }
}
