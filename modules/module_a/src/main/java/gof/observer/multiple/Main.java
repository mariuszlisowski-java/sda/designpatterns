package gof.observer.multiple;

import gof.observer.multiple.observers.ClientA;
import gof.observer.multiple.observers.ClientB;
import gof.observer.multiple.observers.ClientC;
import gof.observer.multiple.observers.Observer;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Observer clientA = new ClientA();
        Observer clientB = new ClientB();
        Observer clientC = new ClientC();

        Subject subject = new Subject();
        subject.subscribe(clientA);
        subject.subscribe(clientB);
        subject.subscribe(clientC);

        subject.startYourJob(); // and notify all subscribers when done

        subject.thread.join();
        subject.unsubscribe(clientA);
        subject.startYourJob();

        subject.thread.join();
        subject.unsubscribe(clientB);
        subject.startYourJob();
    }

}
