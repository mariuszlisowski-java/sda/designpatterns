package gof.observer.multiple;

import gof.observer.multiple.observers.Observer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Subject {
    List<Observer> observers;
    Thread thread;

    public Subject() {
        observers = new ArrayList<>();
    }

    public void subscribe(Observer observer) {
        observers.add(observer);
    }

    public void unsubscribe(Observer observer) {
        observers.remove(observer);
    }

    public void startYourJob() {
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 5; i++) {
                    System.out.println(i);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                // notify all observers
                for (Observer observer : observers) {
                    observer.update();
                    observer.sendMeData(UUID.randomUUID().toString());
                }

            }
        });
        thread.start();
    }

}
