package gof.observer.multiple.observers;

public interface Observer {
    void update();
    void sendMeData(String data);
}
