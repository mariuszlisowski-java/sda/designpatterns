package gof.observer.multiple.observers;

public class ClientC implements Observer {
    @Override
    public void update() {
        System.out.println(this.getClass().getSimpleName() + ": I was notified!");
    }

    @Override
    public void sendMeData(String data) {
        System.out.println(this.getClass().getSimpleName() + ": data received: " + data);
    }
}
