package gof.bridge.drinks.drink;

import gof.bridge.drinks.Taste;

public class Coffee implements Drink {
    @Override
    public String getVolume() {
        return "350ml";
    }

    @Override
    public boolean isAddictive() {
        return true;
    }

    @Override
    public int getNumberOfSugarLumps() {
        return 1;
    }

    @Override
    public Taste getTaste() {
        return Taste.CHOCOLATE;
    }
}
