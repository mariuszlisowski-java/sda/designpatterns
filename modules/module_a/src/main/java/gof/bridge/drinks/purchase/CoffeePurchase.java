package gof.bridge.drinks.purchase;

import gof.bridge.drinks.drink.Coffee;
import gof.bridge.drinks.drink.Drink;

public class CoffeePurchase implements DrinkPurchase {
    @Override
    public Drink buy(Double price) {
        System.out.println("Buying a cup of coffee for " + price);

        return new Coffee();
    }
}
