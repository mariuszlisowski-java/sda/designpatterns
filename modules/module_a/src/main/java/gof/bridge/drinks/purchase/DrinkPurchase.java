package gof.bridge.drinks.purchase;

import gof.bridge.drinks.drink.Drink;

// bridged with Drink interface
public interface DrinkPurchase {
    Drink buy(final Double price);
}
