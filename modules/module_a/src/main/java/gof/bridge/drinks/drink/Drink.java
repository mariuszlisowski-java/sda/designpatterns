package gof.bridge.drinks.drink;

import gof.bridge.drinks.Taste;

public interface Drink {
    String getVolume();
    boolean isAddictive();
    int getNumberOfSugarLumps();
    Taste getTaste();
}
