package gof.bridge.drinks;

public enum Taste {
    BITTER, SWEET, CHOCOLATE, NUT, LEMON
}
