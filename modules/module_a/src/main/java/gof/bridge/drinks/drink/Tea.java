package gof.bridge.drinks.drink;

import gof.bridge.drinks.Taste;
import gof.bridge.drinks.drink.Drink;

public class Tea implements Drink {
    @Override
    public String getVolume() {
        return "250ml";
    }

    @Override
    public boolean isAddictive() {
        return false;
    }

    @Override
    public int getNumberOfSugarLumps() {
        return 2;
    }

    @Override
    public Taste getTaste() {
        return Taste.LEMON;
    }
}
