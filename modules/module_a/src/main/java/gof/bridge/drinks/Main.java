package gof.bridge.drinks;

import gof.bridge.drinks.drink.Drink;
import gof.bridge.drinks.purchase.CoffeePurchase;
import gof.bridge.drinks.purchase.TeaPurchase;

public class Main {

    public static void main(String[] args) {
        Drink cofee = new CoffeePurchase().buy(2.20);
        Drink tea = new TeaPurchase().buy(3.20);
    }

}
