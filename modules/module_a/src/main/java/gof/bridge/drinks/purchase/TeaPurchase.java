package gof.bridge.drinks.purchase;

import gof.bridge.drinks.drink.Drink;
import gof.bridge.drinks.drink.Tea;
import gof.bridge.drinks.purchase.DrinkPurchase;

public class TeaPurchase implements DrinkPurchase {
    @Override
    public Drink buy(Double price) {
        System.out.println("Buying a cup of tea for " + price);

        return new Tea();
    }
}
