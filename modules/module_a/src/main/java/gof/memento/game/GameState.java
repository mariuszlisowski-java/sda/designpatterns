package gof.memento.game;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class GameState {
    private Integer health;
    private Integer money;
    private List<String> items;

    public void heal() {
        health = 100;
    }

    public void takeDamage(final int damage) {
        health -= damage;
    }

    public void addItem(final String item) {
        items.add(item);
    }

    public void loseAllItems() {
        items.clear();
    }

    public void restoreFromSnapshot(final GameStateSnapshot snapshot) {
        health = snapshot.getHealth();
        money = snapshot.getMana();
        items = List.copyOf(snapshot.getItems());
    }
}
