package gof.memento.game;

import java.util.ArrayList;

public class MementoUsage {

    public static void main(String[] args) {
        final GameState gameState = new GameState(100, 80, new ArrayList<>());
        final GameStateManager gameStateManager = new GameStateManager();

        gameStateManager.saveGame(gameState);
        System.out.println(gameState);

        gameState.addItem("Sword");
        gameState.takeDamage(10);
        System.out.println(gameState);

        // first save
        gameStateManager.saveGame(gameState);

        gameState.takeDamage(50);
        gameState.addItem("Shield");
        System.out.println(gameState);

        // second save (latest)
        gameStateManager.saveGame(gameState);

        // decided to load previous save twice
        // second (latest save ommited)
        gameStateManager.restorePreviousCheckpoint();
        // first (applied)
        final GameStateSnapshot gameStateSnapshot = gameStateManager.restorePreviousCheckpoint();
        gameState.restoreFromSnapshot(gameStateSnapshot);
        System.out.println(gameState);
    }

}
