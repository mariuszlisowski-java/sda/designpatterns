package gof.memento.game;

import lombok.Data;

import java.util.List;

@Data
public class GameStateSnapshot {
    private Integer health;
    private Integer mana;
    private List<String> items;

    public GameStateSnapshot(GameState gameState) {
        this.health = gameState.getHealth();
        this.mana = gameState.getMoney();
        this.items = gameState.getItems();
    }
}
