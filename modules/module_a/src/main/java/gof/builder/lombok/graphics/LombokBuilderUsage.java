package gof.builder.lombok.graphics;

public class LombokBuilderUsage {

    public static void main(String[] args) {
        GraphicsCard gc = GraphicsCard.builder()
                .memoryInMb(4096)
                .producer("Dell")
                .series("Optiplex")
                .modelName("D2300")
                .build();
    }

}