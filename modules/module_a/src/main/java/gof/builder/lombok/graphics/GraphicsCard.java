package gof.builder.lombok.graphics;

import lombok.*;

/*
 * @Data - generuje gettery, settery wszystkich pól, metodę toString, a także metody equals wraz z hashCode
 * @NoArgsConstructor - generuje konstruktor bezargumentowy
 * @AllArgsConstructor - generuje konstruktor dla wszystkich pól klasy
 * BBuiler - generuje buildera
 */

@Data
@NoArgsConstructor
//@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Builder
//@Builder(builderMethodName = "builder")
public class GraphicsCard {
    private int memoryInMb;
    private String producer;
    private String series;
    private String modelName;
}

