package gof.builder.insideclass.weapon;

import java.util.ArrayList;
import java.util.List;

public class Weapon {
    private String type;
    private String name;
    private Integer damage;
    private Long durability;
    private List<String> perks;

    // private constructor
    private Weapon(String type, String name, Integer damage, Long durability, List<String> perks) {
        this.type = type;
        this.name = name;
        this.damage = damage;
        this.durability = durability;
        this.perks = perks;
    }

    // static builder
    public static class Builder {
        private String type;
        private String name;
        private Integer damage;
        private Long durability;
        private List<String> perks = new ArrayList<>();

        // default constructor
        // not implemented here

        // building methods
        public Builder withType(final String type) {
            this.type = type;
            return this;
        }

        public Builder withName(final String name) {
            this.name = name;
            return this;
        }

        public Builder withDamage(final Integer damage) {
            this.damage = damage;
            return this;
        }

        public Builder withDurability(final Long durability) {
            this.durability = durability;
            return this;
        }

        public Builder withPerks(final List<String> perks) {
            this.perks = perks;
            return this;
        }

        public Weapon build() {
            return new Weapon(type, name, damage, durability, perks);
        }
    }

}
