package gof.builder.insideclass.weapon;

import java.util.Arrays;

public class WeaponUsage {

    public static void main(String[] args) {
        Weapon riffle = new Weapon.Builder()
                .withName("weapon")
                .withType("riffle")
                .withDamage(0)
                .withDurability(100L)
                .withPerks(Arrays.asList("tough", "light", "accurate"))
                .build();

        Weapon gun = new Weapon.Builder()
                .withName("gun")
                .withDurability(200L)
                .build();

        // cannot crate without builder (private constructor)
//        Weapon weapon = new Weapon();
    }

}
