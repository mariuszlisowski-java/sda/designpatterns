package gof.builder.honestit.homework.url;

public class UrlApp {

    public static void main(String[] args) {
        String address = new UrlBuilder()
                .protocol("http")
                .domain("localhost")
                .port(8080)
                .path("/api/users/search")
                .parameter("q", "firstName")
                .parameter("ordered", null)
                .build();

        System.out.println(address);
    }

}
