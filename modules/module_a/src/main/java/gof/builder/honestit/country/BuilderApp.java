package gof.builder.honestit.country;

import gof.builder.honestit.country.advanced.AdvancedCountryBuilder;
import gof.builder.honestit.country.simple.CityBuilder;
import gof.builder.honestit.country.simple.CountryBuilder;

import java.util.ArrayList;

public class BuilderApp {

    public static void main(String[] args) {

        Country usa = new Country("USA");
        usa.setCapital(new City("Washington"));
        usa.setPopulation(350_000_000L);
        usa.setCities(new ArrayList<City>() {{
            add(new City("New Your"));
            add(new City("Miami"));
            add(new City("Poland"));
        }});

        City seattle = new City("Seattle");
        seattle.setPopulation(800_000L);
        seattle.setCountry(usa);

        Country poland = new Country("Poland");
        poland.setCities(new ArrayList<>());
        poland.getCities().add(new City("Krakow"));
        poland.setCapital(new City("Warszawa"));

        /*
            builder
         */
        // regular
        Country germany = new CountryBuilder()
                .name("Germany")
                .capital(new City("Berlin"))
                .city(new City("Dortmund"))
                .city(new CityBuilder().name("Essen").population(150_000L).build())
                .population(80_000_000L)
                .build();

        City venlo = new CityBuilder()
                .name("Venlo")
                .population(250_000L)
                .country(germany)
                .build();
        // advanced
        Country france = new AdvancedCountryBuilder()
                .name("France")
                .city().name("Paris").population(8_000_000L).asCapital()
                .city().name("Lion").population(1_200_000L).asCity()
                .city().name("Calais").population((700_000L)).asCity()
                .build();

    }
}
