package gof.builder.honestit.homework.url;

import java.util.HashMap;
import java.util.Map;

public class UrlBuilder {
    private Url url = new Url();

    public UrlBuilder protocol(String protocol) {
        url.setProtocol(protocol);
        return this;
    }

    public UrlBuilder domain(String domain) {
        url.setDomain(domain);
        return this;
    }

    public UrlBuilder port(Integer port) {
        url.setPort(port);
        return this;
    }

    public UrlBuilder path(String path) {
        url.setPath(path);
        return this;
    }

    public UrlBuilder parameter(String key, String value) {
        if (url.getParameters() == null) {
            url.setParameters(new HashMap<>());
        }
        url.getParameters().put(key, value);
        return this;
    }

    public String build() {
        if (url.getProtocol() == null ||
            url.getDomain() == null)
        {
            throw new IllegalStateException("Protocol or domain missing");
        }
        StringBuilder completedUrl = new StringBuilder(url.getProtocol() + "://" +
                url.getDomain());
        if (url.getPort() != null) {
            completedUrl.append(":").append(url.getPort());
        }
        if (url.getPath() != null) {
            completedUrl.append(url.getPath());
        } else {
            completedUrl.append("/");
        }
        if (url.getParameters() != null) {
            completedUrl.append("?");
            boolean first = true;
            for (Map.Entry<String, String> pair : url.getParameters().entrySet()) {
                if (first == false) {
                    completedUrl.append("&");
                } else {
                    first = false;
                }
                completedUrl.append(pair.getKey());
                if (pair.getValue() != null) {
                    completedUrl.append("=").append(pair.getValue());
                }
            }
        }

        return completedUrl.toString();
    }
}

