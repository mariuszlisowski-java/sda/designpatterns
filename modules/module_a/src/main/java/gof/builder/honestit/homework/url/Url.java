package gof.builder.honestit.homework.url;

import java.util.Map;

public class Url {
    private String protocol;
    private String domain;
    private Integer port;
    private String path;
    private Map<String, String> parameters;

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getDomain() {
        return domain;
    }

    public Integer getPort() {
        return port;
    }

    public String getPath() {
        return path;
    }
}

/*
 http://localhost:8080/api/users/search?q=firstName&value=Jan&ordered

 http
 ://
 localhost
 :8080
 /api/users/search
 ?                      // begin of parameters
 q=firstName            // key-value
 &                      // separator
 value=Jan              // key-value
 &                      // separator
 ordered                // key

*/

