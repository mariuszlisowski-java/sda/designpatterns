package gof.builder.outsideclass.toy;

public class ToyUsage {

    public static void main(String[] args) {
        Toy formula = new ToyBuilder()
                .withName("car")
                .withType("formula")
                .withMadeOf("metal")
                .build();
    }

}
