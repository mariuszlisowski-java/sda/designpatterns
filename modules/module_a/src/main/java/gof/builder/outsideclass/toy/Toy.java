package gof.builder.outsideclass.toy;

public class Toy {
    private String name;
    private String type;
    private String madeOf;

    // package provate constructor
    Toy(String name, String type, String madeOf) {
        this.name = name;
        this.type = type;
        this.madeOf = madeOf;
    }


}
