package gof.flyweitht.engine;

public enum EngineType {
    DIESEL, GASOLINE, ELECTRIC
}
