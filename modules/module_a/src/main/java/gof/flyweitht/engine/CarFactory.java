package gof.flyweitht.engine;

import java.util.List;

public class CarFactory {
    private static final List<Engine> ENGINES = List.of(
            new Engine("MPI", 1.6D, EngineType.DIESEL),
            new Engine("GTI", 2.0D, EngineType.GASOLINE),
            new Engine("GTX", 1.5, EngineType.GASOLINE),
            new Engine("ELX", 0D, EngineType.ELECTRIC)
    );

    /* flightweight */
    // returns reference to an existing engine
    private static Engine addEngineByKey(final String key) {
        return ENGINES.stream()
                .filter(engine -> engine.getIdentifier().equals(key))
                .findFirst()
                .orElseThrow();
    }

    public Car createVWPolo(final String vin) {
        return new Car("VW", vin, "Polo", addEngineByKey("MPI"));
    }

    public Car createVWPoloGTI(final String vin) {
        return new Car("VW", vin, "PoloGTI", addEngineByKey("GTI"));
    }

    public Car createVWGolf(final String vin) {
        return new Car("VW", vin, "Golf", addEngineByKey("GTX"));
    }

    public Car createSkodaCityGo(final String vin) {
        return new Car("Skoda", vin, "CityGO", addEngineByKey("ELX"));
    }
}
