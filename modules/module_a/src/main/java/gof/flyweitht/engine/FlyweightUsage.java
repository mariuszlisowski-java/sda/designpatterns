package gof.flyweitht.engine;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class FlyweightUsage {
    private static final int ORDERED_CARS = 1000;
    private static final int ENGINE_MODELS = 4;

    public static void main(String[] args) {
        final CarFactory carFactory = new CarFactory();
        final List<Car> manufacturedCars = new ArrayList<>(ORDERED_CARS);

        for (int idx = 0; idx < ORDERED_CARS; idx++) {
            switch (new Random().nextInt(ENGINE_MODELS)) {
                case 0:
                    manufacturedCars.add(carFactory.createSkodaCityGo(UUID.randomUUID().toString()));
                    break;
                case 1:
                    manufacturedCars.add(carFactory.createVWGolf(UUID.randomUUID().toString()));
                    break;
                case 2:
                    manufacturedCars.add(carFactory.createVWPolo(UUID.randomUUID().toString()));
                    break;
                case 3:
                    manufacturedCars.add(carFactory.createVWPoloGTI(UUID.randomUUID().toString()));
                    break;
            }
        }
        // every car has an engine but only 4 created
        System.out.println("Car objects created: " + manufacturedCars.size() +
                           "\nReferences to Engine object: " + Engine.instances +
                           " only");
    }
}
