package gof.flyweitht.engine;

public class Engine {
    public static int instances = 0;

    private String identifier;
    private Double volume;
    private EngineType engineType;

    public Engine(String identifier, Double volume, EngineType engineType) {
        this.identifier = identifier;
        this.volume = volume;
        this.engineType = engineType;
        instances++;
    }

    public String getIdentifier() {
        return identifier;
    }
}
