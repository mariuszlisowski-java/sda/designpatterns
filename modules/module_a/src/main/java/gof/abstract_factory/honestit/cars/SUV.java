package gof.abstract_factory.honestit.cars;

public class SUV extends Car {

    public SUV() {
        this.setType("SUV");
    }
}
