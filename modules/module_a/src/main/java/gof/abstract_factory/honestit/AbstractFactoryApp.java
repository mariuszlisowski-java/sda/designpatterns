package gof.abstract_factory.honestit;

import gof.abstract_factory.honestit.cars.Car;
import gof.abstract_factory.honestit.cars.Combi;
import gof.abstract_factory.honestit.cars.SUV;
import gof.abstract_factory.honestit.cars.Sedan;
import gof.abstract_factory.honestit.factories.CarFactory;
import gof.abstract_factory.honestit.factories.FiatFactory;
import gof.abstract_factory.honestit.factories.SkodaFactory;

public class AbstractFactoryApp {

    public static void main(String[] args) {
        Car fiatCombi = new Combi();
        fiatCombi.setKM("140");
        fiatCombi.setVolume("1.8");
        fiatCombi.setPrice("50.000");

        Car fiatSUV = new SUV();
        fiatSUV.setKM("180");
        fiatSUV.setVolume("2.0");
        fiatSUV.setPrice("80.000");

        Car fiatSedan = new Sedan();
        fiatSedan.setKM("130");
        fiatSedan.setVolume("1.4");
        fiatSedan.setPrice("40.000");

        /*
            abstract factory
         */

        // fiat factory
        CarFactory fiatFactory = new FiatFactory();
        Car newFiatCombi = fiatFactory.combi();
        Car newFiatSUV = fiatFactory.suv();
        Car newFiatSedan = fiatFactory.sedan();

        // skoda factory
        CarFactory skodaFactory = new SkodaFactory();
        Car newSkodaCombi = skodaFactory.combi();
        Car newSkodaSUV = skodaFactory.suv();
        Car newSkodaSedan = skodaFactory.sedan();

        // custom factory (enum change only)
        Car newestFiatSedan = CarFactory.getFactory(CarFactory.Made.FIAT).sedan();
        Car newestSkodaSedan = CarFactory.getFactory(CarFactory.Made.SKODA).sedan();

        // default
        CarFactory defaultFactory = CarFactory.getDefaultFactory();
        Car defaultCar = defaultFactory.suv();
        System.out.println(defaultCar);

    }
}
