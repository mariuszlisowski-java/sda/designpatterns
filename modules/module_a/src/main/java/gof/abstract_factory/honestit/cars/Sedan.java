package gof.abstract_factory.honestit.cars;

public class Sedan extends Car {

    public Sedan() {
        this.setType("Sedan");
    }
}
