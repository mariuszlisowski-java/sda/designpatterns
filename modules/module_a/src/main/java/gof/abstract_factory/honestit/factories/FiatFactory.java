package gof.abstract_factory.honestit.factories;

import gof.abstract_factory.honestit.cars.Car;
import gof.abstract_factory.honestit.cars.Combi;
import gof.abstract_factory.honestit.cars.SUV;
import gof.abstract_factory.honestit.cars.Sedan;

public class FiatFactory extends CarFactory {

    @Override
    public Combi combi() {
//        Combi combi = fiat(new Combi());
        Combi combi = new Combi();
        combi.setMade("Fiat");
        combi.setKM("140 KM");
        combi.setVolume("1.8");
        combi.setPrice("50.000");
        combi.setName("Fiat Kombi");
        return combi;
    }

    @Override
    public SUV suv() {
//        SUV suv = fiat(new SUV());
        SUV suv = new SUV();
        suv.setMade("Fiat");
        suv.setKM("180 KM");
        suv.setVolume("2.0");
        suv.setPrice("80.000");
        suv.setName("Fiat SUV");
        return suv;
    }

    @Override
    public Sedan sedan() {
//        Sedan sedan = fiat(new Sedan());
        Sedan sedan = new Sedan();
        sedan.setName("Fiat");
        sedan.setKM("130 KM");
        sedan.setVolume("1.4");
        sedan.setPrice("40.000");
        sedan.setName("Fiat Sedan");
        return sedan;
    }
    // DRY
    private <T extends Car> T fiat(T car) {
        car.setMade("Fiat");
        return car;
    }
}
