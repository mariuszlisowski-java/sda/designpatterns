package gof.abstract_factory.honestit.cars;

public class Combi extends Car {

    public Combi() {
        this.setType("Kombi");
    }
}
