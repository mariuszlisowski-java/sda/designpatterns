package gof.abstract_factory.cars.factory;

import gof.abstract_factory.cars.Car;

public interface CarFactory {
    Car manufactureSedan();
    Car manufactureCombi();
    Car manufactureHatchback();
}
