package gof.abstract_factory.cars;

import gof.abstract_factory.cars.factory.CarFactory;
import gof.abstract_factory.cars.provider.CarModel;
import gof.abstract_factory.cars.provider.FactoryProvider;

public class Main {

    public static void main(String[] args) {
        CarFactory a4Factory = FactoryProvider.createFactory(CarModel.AUDI_A4);
        Car a4Combi = a4Factory.manufactureCombi();
        Car a4Sedan = a4Factory.manufactureSedan();
        Car a4Hatch = a4Factory.manufactureHatchback();

        CarFactory corollaFactory = FactoryProvider.createFactory(CarModel.TOYOTA_COROLLA);
        Car corollaCombi = corollaFactory.manufactureCombi();
        Car corollaSedan = corollaFactory.manufactureSedan();
        Car corollaHatch = corollaFactory.manufactureHatchback();
    }
}
