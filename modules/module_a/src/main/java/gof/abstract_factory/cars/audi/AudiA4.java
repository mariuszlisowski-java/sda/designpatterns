package gof.abstract_factory.cars.audi;

import gof.abstract_factory.cars.Car;

public abstract class AudiA4 implements Car {
    @Override
    public String getModelName() {
        return "A4";
    }

    @Override
    public String getManufacturer() {
        return "Audi";
    }
}
