package gof.abstract_factory.cars.provider;

public enum CarModel {
    TOYOTA_COROLLA,
    AUDI_A4
}
