package gof.abstract_factory.cars.toyota;

import gof.abstract_factory.cars.Type;

public class ToyotaCorollaHatchback extends ToyotaCorolla {
    @Override
    public Type getType() {
        return Type.HATCHBACK;
    }

    @Override
    public Integer getCylindersNum() {
        return 4;
    }

    @Override
    public Float getEngineVolume() {
        return 1.8F;
    }

    @Override
    public Integer getTrunkSize() {
        return 420;
    }
}
