package gof.abstract_factory.cars;

public interface Car {
    Type getType();
    String getModelName();
    Integer getCylindersNum();
    String getManufacturer();
    Float getEngineVolume();
    Integer getTrunkSize();
}
