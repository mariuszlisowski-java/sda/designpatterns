package gof.abstract_factory.cars.audi;

import gof.abstract_factory.cars.Type;

public class AudiA4Sedan extends AudiA4 {
    @Override
    public Type getType() {
        return Type.SEDAN;
    }

    @Override
    public Integer getCylindersNum() {
        return 6;
    }

    @Override
    public Float getEngineVolume() {
        return 2.7F;
    }

    @Override
    public Integer getTrunkSize() {
        return 400;
    }
}
