package gof.abstract_factory.cars.factory;

import gof.abstract_factory.cars.Car;
import gof.abstract_factory.cars.audi.AudiA4Combi;
import gof.abstract_factory.cars.audi.AudiA4Hatchback;
import gof.abstract_factory.cars.audi.AudiA4Sedan;

public class AudiA4Factory implements CarFactory {
    @Override
    public Car manufactureSedan() {
        return new AudiA4Sedan();
    }

    @Override
    public Car manufactureCombi() {
        return new AudiA4Combi();
    }

    @Override
    public Car manufactureHatchback() {
        return new AudiA4Hatchback();
    }
}
