package gof.abstract_factory.cars.factory;

import gof.abstract_factory.cars.Car;
import gof.abstract_factory.cars.toyota.ToyotaCorollaCombi;
import gof.abstract_factory.cars.toyota.ToyotaCorollaHatchback;
import gof.abstract_factory.cars.toyota.ToyotaCorollaSedan;

public class ToyotaCorollaFactory implements CarFactory {
    @Override
    public Car manufactureSedan() {
        return new ToyotaCorollaSedan();
    }

    @Override
    public Car manufactureCombi() {
        return new ToyotaCorollaCombi();
    }

    @Override
    public Car manufactureHatchback() {
        return new ToyotaCorollaHatchback();
    }
}
