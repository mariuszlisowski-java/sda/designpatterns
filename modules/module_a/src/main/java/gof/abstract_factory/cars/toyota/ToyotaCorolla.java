package gof.abstract_factory.cars.toyota;

import gof.abstract_factory.cars.AbstractCar;

public abstract class ToyotaCorolla extends AbstractCar {
    @Override
    public String getModelName() {
        return "Corolla";
    }

    @Override
    public String getManufacturer() {
        return "Toyota";
    }
}
