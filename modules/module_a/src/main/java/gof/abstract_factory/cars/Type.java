package gof.abstract_factory.cars;

public enum Type {
    SEDAN, HATCHBACK, COMBI
}
