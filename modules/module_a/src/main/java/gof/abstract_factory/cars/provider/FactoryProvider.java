package gof.abstract_factory.cars.provider;

import gof.abstract_factory.cars.factory.AudiA4Factory;
import gof.abstract_factory.cars.factory.CarFactory;
import gof.abstract_factory.cars.factory.ToyotaCorollaFactory;

import java.security.InvalidParameterException;

public class FactoryProvider {
    public static CarFactory createFactory(final CarModel model) {
        switch (model) {
            case TOYOTA_COROLLA:
                return new ToyotaCorollaFactory();
            case AUDI_A4:
                return new AudiA4Factory();
            default:
                throw new InvalidParameterException();
        }
    }
}
