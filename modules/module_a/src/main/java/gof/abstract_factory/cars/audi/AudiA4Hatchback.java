package gof.abstract_factory.cars.audi;

import gof.abstract_factory.cars.Type;

public class AudiA4Hatchback extends AudiA4 {
    @Override
    public Type getType() {
        return Type.HATCHBACK;
    }

    @Override
    public Integer getCylindersNum() {
        return 6;
    }

    @Override
    public Float getEngineVolume() {
        return 3.0F;
    }

    @Override
    public Integer getTrunkSize() {
        return 480;
    }
}
