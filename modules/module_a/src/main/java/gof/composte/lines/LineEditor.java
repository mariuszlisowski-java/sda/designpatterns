package gof.composte.lines;

import gof.composte.lines.compositions.CompoundLine;
import gof.composte.lines.compositions.DottedLine;
import gof.composte.lines.compositions.Line;
import gof.composte.lines.compositions.SolidLine;

import java.security.InvalidParameterException;
import java.util.List;

public class LineEditor {
    private final CompoundLine compoundLine = new CompoundLine();

    public void selectAndRemove(final List<Line> lines) {
        lines.forEach(compoundLine::removeLine);
    }

    public void createLine(final String type) {
        switch (type) {
            case "dotted":
                compoundLine.addLine(new DottedLine()); break;
            case "solid":
                compoundLine.addLine(new SolidLine()); break;
            default:
                throw new InvalidParameterException("No such line!");
        }
    }

    public void drawAllLinesAtPoint(final Point point, final double length) {
        compoundLine.setStartingPosition(point);
        compoundLine.draw(length);
    }
}
