package gof.composte.lines;

public class Main {

    public static void main(String[] args) {
        LineEditor editor = new LineEditor();

        editor.createLine("solid");
        editor.createLine("dotted");
        editor.drawAllLinesAtPoint(new Point(5, 5), 25);

    }

}
