package gof.composte.lines.compositions;

import gof.composte.lines.Point;

public interface Line {
    void draw(double lengthInPixels);
    void setStartingPosition(Point position);
    Point getStartingPoint();
}
