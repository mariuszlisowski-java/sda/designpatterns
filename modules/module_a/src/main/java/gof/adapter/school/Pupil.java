package gof.adapter.school;

import java.util.List;

// class CANNOT implement Student interface
public class Pupil {
    private final String firstName;
    private final String lastName;
    private final String email;
    private final Integer age;
    private final List<Integer> grades;

    public Pupil(String firstName, String lastName, String email, Integer age, List<Integer> grades) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.age = age;
        this.grades = grades;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public Integer getAge() {
        return age;
    }

    public List<Integer> getGrades() {
        return grades;
    }
}
