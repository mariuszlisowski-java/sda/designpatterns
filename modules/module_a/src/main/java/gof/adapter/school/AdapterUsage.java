package gof.adapter.school;

import java.util.ArrayList;
import java.util.List;

public class AdapterUsage {

    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<>();

        Pupil andySmith =
                new Pupil("Andy", "Smith", "a.smith@host.io", 13, List.of(4, 4, 5, 6));
        studentList.add(new PupilAdapter(andySmith));

        studentList.stream()
                .forEach(student -> {
                    System.out.println(student.getFullName());
                    System.out.println(student.isAdult() ? "adult" : "not adult");
                    System.out.println(student.getContactDetails());
                    System.out.println(student.getResults());
                });
    }

}
