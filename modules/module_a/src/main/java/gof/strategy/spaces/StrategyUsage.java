package gof.strategy.spaces;

import gof.strategy.spaces.strategies.SpacesModificationStrategy;

public class StrategyUsage {

    public static void main(String[] args) {
        final StrategyType strategyType = StrategyType.REPLACE;
        final String input = "Hello from SDA knowledge base!";

        SpacesModificationStrategyProvider provider = new SpacesModificationStrategyProvider();
        SpacesModificationStrategy strategy = provider.getStrategy(strategyType);

        final String output = strategy.modify(input);
        System.out.println("Output: " + output);
    }

}
