package gof.strategy.spaces;

import gof.strategy.spaces.strategies.DoubleSpacesStrategy;
import gof.strategy.spaces.strategies.RemoveSpacesStrategy;
import gof.strategy.spaces.strategies.ReplaceWithUnderscoreStrategy;
import gof.strategy.spaces.strategies.SpacesModificationStrategy;

public class SpacesModificationStrategyProvider {
    public SpacesModificationStrategy getStrategy(final StrategyType strategyType) {
        switch (strategyType) {
            case DOUBLE:
                return new DoubleSpacesStrategy();
            case REMOVE:
                return new RemoveSpacesStrategy();
            case REPLACE:
                return new ReplaceWithUnderscoreStrategy();
        }
        throw new UnsupportedOperationException("Unsupported strategy type");
    }
}
