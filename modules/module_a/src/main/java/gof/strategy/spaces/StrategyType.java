package gof.strategy.spaces;

public enum StrategyType {
    DOUBLE,
    REMOVE,
    REPLACE
}
