package gof.strategy.spaces.strategies;

public interface SpacesModificationStrategy {
    String modify(String input);
}
