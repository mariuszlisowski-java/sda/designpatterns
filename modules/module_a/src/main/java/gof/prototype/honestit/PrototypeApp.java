package gof.prototype.honestit;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PrototypeApp {

    public static void main(String[] args) {
        Letter letterA = new Letter();
        letterA.setTitle("Any title");
        letterA.setContent("Any content");
        letterA.setDate(LocalDate.now());
        letterA.setReceiver("Any receiver");

        Letter letterB = new Letter();
        letterB.setTitle("Same title");
        letterB.setContent("Same content");
        letterB.setDate(LocalDate.now());
        letterB.setReceiver("Different receiver");

        Letter letterC = new Letter();
        letterC.setTitle("Same title");
        letterC.setContent("Same content");
        letterC.setDate(LocalDate.now());
        letterC.setReceiver("Unknown receiver");

        /*
            prototype
         */
        // make template once
        Template template = new Template();
        template.setTitle("Any title");
        template.setContent("Any content");
        template.setDate(LocalDate.now());
        template.setReceiver("Any receiver");
        // then use it many times
        Letter newLetterA = template.letter();
        Letter newLetterB = template.letter("Known receiver");
        Letter newLetterC = template.letter("Suspicous receiver");

        // possible usage
        List<Letter> letters = Arrays.asList("Tony", "Becky", "Tim")
                .stream()
                .map(receiver -> template.letter(receiver))
                .collect(Collectors.toList());

        letters.forEach(System.out::println);
    }

}
