package gof.prototype.code;

public class CodeFileApp {

    public static void main(String[] args) throws CloneNotSupportedException {
        final CodeFilesPrototype filePrototype = new CodeFilesPrototype();

        CodeFile sourceFileA = filePrototype.createCodeFile("sourceA");
        CodeFile sourceFileB = filePrototype.createCodeFile("sourceB");
        CodeFile sourceFileC = filePrototype.createCodeFile("sourceC");
    }
}
