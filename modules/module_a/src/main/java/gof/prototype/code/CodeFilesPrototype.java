package gof.prototype.code;

public class CodeFilesPrototype {
    private static final CodeFile BASE_FILE = new CodeFile("MIT licence", "cpp");

    public CodeFile createCodeFile(String filename) throws CloneNotSupportedException {
//        BASE_FILE.setFileName(filename);

        // deep copy because string is immutable
        CodeFile file = BASE_FILE.createClone();        // usage of prototype
        file.setFileName(filename);
        return file;
    }

}
