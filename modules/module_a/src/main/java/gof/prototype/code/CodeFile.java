package gof.prototype.code;

public class CodeFile implements Cloneable {
    private String licenseContent;
    private String fileName;
    private String fileExtension;


    public CodeFile(String licence, String extension) {
        this.licenseContent = licence;
        this.fileExtension = extension;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    // shallow copy
    public CodeFile createClone() throws CloneNotSupportedException {
        return (CodeFile) clone();
    }

    @Override
    public String toString() {
        return "CodeFile{" +
                "licenseContent='" + licenseContent + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fileExtension='" + fileExtension + '\'' +
                '}';
    }
}
