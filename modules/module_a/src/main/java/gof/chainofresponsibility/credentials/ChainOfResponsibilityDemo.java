package gof.chainofresponsibility.credentials;

import gof.chainofresponsibility.credentials.handlers.AuthenticationHandler;
import gof.chainofresponsibility.credentials.handlers.AwsAuthenticationHandler;
import gof.chainofresponsibility.credentials.handlers.BearerTokenAuthenticationHandler;
import gof.chainofresponsibility.credentials.handlers.UsernameAndPasswordAuthenticationHandler;
import gof.chainofresponsibility.credentials.signin.AwsSignature;
import gof.chainofresponsibility.credentials.signin.BearerToken;
import gof.chainofresponsibility.credentials.signin.UsernameAndPasswordCredentials;

public class ChainOfResponsibilityDemo {

    public static void main(String[] args) {
        final AuthenticationHandler authenticationHandlerUnP = new UsernameAndPasswordAuthenticationHandler();
        final AuthenticationHandler authenticationHandlerBearer = new BearerTokenAuthenticationHandler();
        final AuthenticationHandler authenticationHandlerAws = new AwsAuthenticationHandler();

        final ChainAuthenticationElement lastElement =
                new ChainAuthenticationElement(authenticationHandlerAws);
        final ChainAuthenticationElement middleElement =
                new ChainAuthenticationElement(authenticationHandlerBearer, lastElement);
        final ChainAuthenticationElement firstElement =
                new ChainAuthenticationElement(authenticationHandlerUnP, middleElement);

        firstElement.authenticate(new AwsSignature());
        firstElement.authenticate(new UsernameAndPasswordCredentials());
        firstElement.authenticate(new BearerToken());

    }

}
