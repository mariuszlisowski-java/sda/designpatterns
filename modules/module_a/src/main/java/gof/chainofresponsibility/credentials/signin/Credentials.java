package gof.chainofresponsibility.credentials.signin;

public interface Credentials {
    String getCredentials(String userId);
}
