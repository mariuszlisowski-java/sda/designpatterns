package gof.chainofresponsibility.credentials.signin;

import java.util.UUID;

public class AwsSignature implements Credentials {
    @Override
    public String getCredentials(String userId) {
        return UUID.randomUUID().toString(); // dummy implementation
    }
}
