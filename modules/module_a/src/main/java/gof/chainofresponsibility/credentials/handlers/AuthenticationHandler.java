package gof.chainofresponsibility.credentials.handlers;

import gof.chainofresponsibility.credentials.signin.Credentials;

public interface AuthenticationHandler {
    boolean authenticate(Credentials credentials);
    boolean supports(Class<?> clazz);
}
