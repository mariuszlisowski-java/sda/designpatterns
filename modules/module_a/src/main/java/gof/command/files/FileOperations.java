package gof.command.files;

import gof.command.files.commands.ChangeClassNameCommand;
import gof.command.files.commands.ChangeFileNameCommand;
import gof.command.files.commands.Command;

public class FileOperations {
    public static void main(String[] args) {
        final JavaFile javaFile = new JavaFile("Commands.java", "Commands");

        final Command changeFileNameCommand = new ChangeFileNameCommand(javaFile, "Orders.java");
        final Command changeClassNameCommand = new ChangeClassNameCommand(javaFile, "Orders");

        // original file
        System.out.println(javaFile);
        changeClassNameCommand.cancel();

        /* command */
        // filename changed
        changeFileNameCommand.apply();
        changeClassNameCommand.apply();
        System.out.println(javaFile);

        // change cancelled
        changeFileNameCommand.cancel();
        changeClassNameCommand.cancel();
        System.out.println(javaFile);

    }

}
