package gof.command.files.commands;

import gof.command.files.JavaFile;

public class ChangeFileNameCommand implements Command {
    private final JavaFile javaFile;
    private final String newName;
    private String previousName = null;

    public ChangeFileNameCommand(JavaFile javaFile, String newName) {
        this.javaFile = javaFile;
        this.newName = newName;
    }

    @Override
    public void apply() {
        if (javaFile.getFileName().equals(newName)) {
            System.out.println("# File name still the same!");
        } else {
            previousName = javaFile.getFileName();
            javaFile.setFileName(newName);
            System.out.println("# File name changed to " + newName);
        }
    }

    @Override
    public void cancel() {
        if (previousName == null) {
            System.out.println("# Nothing to cancel!");
        } else {
            javaFile.setFileName(previousName);
            System.out.println("# File name changed to previous, i.e. " + previousName);
            previousName = null;
        }
    }
}
