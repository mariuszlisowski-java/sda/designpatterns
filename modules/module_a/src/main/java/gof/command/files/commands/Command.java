package gof.command.files.commands;

public interface Command {
    void apply();
    void cancel();
}
