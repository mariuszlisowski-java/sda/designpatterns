package gof.command.files.commands;

import gof.command.files.JavaFile;

public class ChangeClassNameCommand implements Command {
    private final JavaFile javaFile;
    private final String newName;
    private String previousName = null;

    public ChangeClassNameCommand(JavaFile javaFile, String newName) {
        this.javaFile = javaFile;
        this.newName = newName;
    }

    @Override
    public void apply() {
        if (javaFile.getFileName().equals(newName)) {
            System.out.println("# Class name still the same!");
        } else {
            previousName = javaFile.getClassName();
            javaFile.setClassName(newName);
            System.out.println("# Class name changed to " + newName);
        }
    }

    @Override
    public void cancel() {
        if (previousName == null) {
            System.out.println("# Nothing to cancel!");
        } else {
            javaFile.setClassName(previousName);
            System.out.println("# Class name changed to previous, i.e. " + previousName);
            previousName = null;
        }
    }
}
