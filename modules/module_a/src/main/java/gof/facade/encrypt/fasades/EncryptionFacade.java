package gof.facade.encrypt.fasades;

import gof.facade.encrypt.Encryptors;

public class EncryptionFacade implements Encryptors {
    private final BCryptEncryptor bCryptEncryptor;
    private final SCryptEncryptor sCryptEncryptor;

    public EncryptionFacade(BCryptEncryptor bCryptEncryptor, SCryptEncryptor sCryptEncryptor) {
        this.bCryptEncryptor = bCryptEncryptor;
        this.sCryptEncryptor = sCryptEncryptor;
    }

    @Override
    public String encryptWithBCrypt(String toEncrypt) {
        return bCryptEncryptor.encrypt(toEncrypt);
    }

    @Override
    public String encryptWithSCrypt(String toEncrypt) {
        return sCryptEncryptor.encrypt(toEncrypt);
    }
}
