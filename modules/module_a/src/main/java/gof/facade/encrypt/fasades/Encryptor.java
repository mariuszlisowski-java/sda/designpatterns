package gof.facade.encrypt.fasades;

public interface Encryptor {
    String encrypt(String toEncrypt);
}
