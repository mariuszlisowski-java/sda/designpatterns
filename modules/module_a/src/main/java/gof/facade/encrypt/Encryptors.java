package gof.facade.encrypt;

public interface Encryptors {
    String encryptWithBCrypt(final String toEncrypt);
    String encryptWithSCrypt(final String toEncrypt);
}
