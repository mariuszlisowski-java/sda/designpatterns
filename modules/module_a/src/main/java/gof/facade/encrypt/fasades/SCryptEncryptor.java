package gof.facade.encrypt.fasades;

public class SCryptEncryptor implements Encryptor {
    @Override
    public String encrypt(String toEncrypt) {
        return "encrypting " + toEncrypt + " with SCrypt";
    }
}
