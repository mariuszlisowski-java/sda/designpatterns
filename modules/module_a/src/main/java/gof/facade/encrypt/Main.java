package gof.facade.encrypt;

import gof.facade.encrypt.fasades.BCryptEncryptor;
import gof.facade.encrypt.fasades.EncryptionFacade;
import gof.facade.encrypt.fasades.SCryptEncryptor;

public class Main {

    public static void main(String[] args) {
        BCryptEncryptor bCrypt = new BCryptEncryptor();
        SCryptEncryptor sCrypt = new SCryptEncryptor();

        /* facade */
        EncryptionFacade encrypotors = new EncryptionFacade(bCrypt, sCrypt);

        String bCrypted = encrypotors.encryptWithBCrypt("blabla");
        String sCrypted = encrypotors.encryptWithSCrypt("blabla");

        System.out.println(bCrypted);
        System.out.println(sCrypted);

    }

}
