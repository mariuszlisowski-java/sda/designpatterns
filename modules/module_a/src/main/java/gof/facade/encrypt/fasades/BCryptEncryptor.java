package gof.facade.encrypt.fasades;

public class BCryptEncryptor implements Encryptor {
    @Override
    public String encrypt(String toEncrypt) {
        return "encrypting " + toEncrypt + " with BCrypt";
    }
}
