package gof.facade.order;

public interface ProductAvailabilityService {
    boolean isAvailable(Long productId);
}
