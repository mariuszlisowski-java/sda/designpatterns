package gof.facade.order;

public class Main {

    public static void main(String[] args) {
        ProductAvailabilityService availability = new ProductAvailabilityService() {
            @Override
            public boolean isAvailable(Long productId) {
                System.out.println("Product available... click to pay");
                return true;
            }
        };

        PaymentService payment = new PaymentService() {
            @Override
            public void pay(Long productId, int amount) {
                System.out.println("Payment successful!");
            }
        };

        DeliveryService delivery = new DeliveryService() {
            @Override
            public void deliverProduct(Long productsId, int amount, String recipient) {
                System.out.println("Product delivered!");
            }
        };


        /* facade */
        OrderFacade order = new OrderFacade(availability, payment, delivery);

        order.placeOrder(2344533L, 7, "Raisin");


    }

}
