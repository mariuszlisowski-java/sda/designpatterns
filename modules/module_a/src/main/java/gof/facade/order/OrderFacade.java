package gof.facade.order;

public class OrderFacade {
    private final DeliveryService deliveryService;
    private final PaymentService paymentService;
    private final ProductAvailabilityService productAvailabilityService;

    public OrderFacade(
            ProductAvailabilityService productAvailabilityService,
            PaymentService paymentService,
            DeliveryService deliveryService)
    {
        this.deliveryService = deliveryService;
        this.paymentService = paymentService;
        this.productAvailabilityService = productAvailabilityService;
    }

    public boolean placeOrder(final Long productId, final int amount, final String recipient) {
        if (productAvailabilityService.isAvailable(productId)) {
            paymentService.pay(productId, amount);
            deliveryService.deliverProduct(productId, amount, recipient);
            return true;
        }
        return false;
    }
}
