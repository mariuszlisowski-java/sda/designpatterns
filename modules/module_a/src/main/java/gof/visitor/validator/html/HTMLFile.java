package gof.visitor.validator.html;

import gof.visitor.validator.visitors.Visitor;

public interface HTMLFile {
    String getDoctypeDeclaration();
    String getHead();
    String getBody();
    void accept(Visitor visitor);
}
