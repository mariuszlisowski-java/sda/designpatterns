package gof.visitor.validator.visitors;

import gof.visitor.validator.html.HTML4File;
import gof.visitor.validator.html.HTML5File;
import gof.visitor.validator.html.XHTMLFile;

public class HTMLFileValidator implements Visitor {
    @Override
    public void validateFile(final HTML4File html4File) {
        System.out.println("Validating HTML4 schema with https://validator.w3.org/#validate_by_uri+with_options");
    }

    @Override
    public void validateFile(final HTML5File html5File) {
        System.out.println("Validating HTML5 schema with https://validator.w3.org/#validate_by_uri+with_options");
    }

    @Override
    public void validateFile(final XHTMLFile xhtmlFile) {
        System.out.println("Validating XHTML schema with https://validator.w3.org/#validate_by_uri+with_options");
    }
}
