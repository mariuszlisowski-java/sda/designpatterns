package gof.visitor.validator.visitors;

import gof.visitor.validator.html.HTML4File;
import gof.visitor.validator.html.HTML5File;
import gof.visitor.validator.html.XHTMLFile;

public interface Visitor {
    void validateFile(HTML4File html4File);
    void validateFile(HTML5File html5File);
    void validateFile(XHTMLFile xhtmlFile);
}
