package gof.visitor.honestit.impl.predicates;

import gof.visitor.honestit.impl.Visitor;

import java.io.File;

public class JavaFilesPrintingVisitor implements Visitor {
    @Override
    public void use(File file) {
        System.out.println(file.getName());
    }

    @Override
    public boolean accept(File file) {
        return file.getName().endsWith(".java");
    }
}
