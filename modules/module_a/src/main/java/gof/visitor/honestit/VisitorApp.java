package gof.visitor.honestit;

import gof.visitor.honestit.impl.CountingVisitor;
import gof.visitor.honestit.impl.DirectoryPrintingVisitor;
import gof.visitor.honestit.impl.Traversal;
import gof.visitor.honestit.impl.predicates.ClassFilePredicate;
import gof.visitor.honestit.impl.predicates.JavaFilesPrintingVisitor;

import java.io.File;
import java.util.function.Predicate;

public class VisitorApp {

    public static void main(String[] args) {

        File currentDir = new File(".");

        System.out.println("--- Przechodzimy po katalogu bieżącym");
        traverse(currentDir);
        printDirs(currentDir);
        printJavas(currentDir);

        int result = countClassFile(currentDir, 0);
        System.out.println("Class files: " + result);

        /**
         * Evaluates this predicate on the given argument.
         *
         * @param t the input argument
         * @return {@code true} if the input argument matches the predicate,
         * otherwise {@code false}
         */
        /**
         *  visitor
         */

        // directory printing visitor
        Traversal.traverse(currentDir, new DirectoryPrintingVisitor());
        Traversal.traverse(currentDir, new JavaFilesPrintingVisitor());

        // counting visitor
        CountingVisitor classCountingVisitor = new CountingVisitor(new ClassFilePredicate());
        Traversal.traverse(currentDir, classCountingVisitor);
        System.out.println(".class files: " + classCountingVisitor.getCount());

        Predicate<File> javaFilePredicate = (file) -> file.getName().endsWith(".java");
        CountingVisitor javaFilesVisitor = new CountingVisitor(javaFilePredicate);
        Traversal.traverse(currentDir, javaFilesVisitor);
        System.out.println(".java files: " + javaFilesVisitor.getCount());

        // (inlined predicate)
        CountingVisitor allFilesVisitor = new CountingVisitor(file -> file.isFile());
        Traversal.traverse(currentDir, allFilesVisitor);
        System.out.println("all files: " + allFilesVisitor.getCount());

        // (functional interface)
        Predicate<File> allDirsPredicate = new Predicate<File>() {
            @Override
            public boolean test(File file) {
                return file.isDirectory();
            }
        };
        CountingVisitor allDirsVisitor = new CountingVisitor(allDirsPredicate);
        Traversal.traverse(currentDir, allDirsVisitor);
        System.out.println("all dirs: " + allDirsVisitor.getCount());

    }

    public static void traverse(File root) {
        if (root == null) {
            return;
        }
        if (root.isDirectory()) {
            for (File innerFile : root.listFiles()) {
                traverse(innerFile);
            }
        }
    }

    public static void printDirs(File root) {
        if (root == null) {
            return;
        }
        if (root.isDirectory()) {
            System.out.println(root.getName());
            for (File innerFile : root.listFiles()) {
                printDirs(innerFile);
            }
        }
    }

    public static void printJavas(File root) {
        if (root == null) {
            return;
        }
        if (root.isDirectory()) {
            for (File innerFile : root.listFiles()) {
                printJavas(innerFile);
            }
        } else if (root.getName().endsWith(".java")) {
            System.out.println(root.getName());
        }
    }

    public static int countClassFile(File root, int counter) {
        if (root == null) {
            return 0;
        }
        if (root.isFile() && root.getName().endsWith(".class")) {
            return 1;
        }
        if (root.isDirectory()) {
            for (File innerFile : root.listFiles()) {
                counter += countClassFile(innerFile, 0);
            }
        }

        return counter;
    }
}
