package gof.factory_method.honestit.factories;

import gof.factory_method.honestit.Coach;
import gof.factory_method.honestit.Trainee;

public class TraineeFactory {

    public static Trainee withCoach(String firstName, String lastName, Coach coach) {
        Trainee trainee = new Trainee(firstName, lastName);
        trainee.setCoach(coach);
        return trainee;
    }

    public static Trainee trainee(String firstName, String lastName) {
        Trainee trainee = new Trainee(firstName, lastName);
        return trainee;
    }
}
