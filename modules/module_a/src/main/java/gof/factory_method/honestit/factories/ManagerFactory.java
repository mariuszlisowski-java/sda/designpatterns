package gof.factory_method.honestit.factories;

import gof.factory_method.honestit.Manager;

public class ManagerFactory {

    public static Manager manager(String firstName, String lastName) {
        Manager manager = new Manager();
        manager.setFirstName(firstName);
        manager.setLastName(lastName);
        return manager;
    }
}
