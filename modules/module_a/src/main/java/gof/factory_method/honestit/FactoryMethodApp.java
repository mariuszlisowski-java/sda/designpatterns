package gof.factory_method.honestit;

import gof.factory_method.honestit.factories.CoachFactory;
import gof.factory_method.honestit.factories.ManagerFactory;
import gof.factory_method.honestit.factories.TraineeFactory;

public class FactoryMethodApp {

    public static void main(String[] args) {
        Manager manager = new Manager();
        manager.setFirstName("John");
        manager.setLastName("Smith");

        Coach coachA = new Coach();
        coachA.setFirstName("Tim");
        coachA.setLastName("Peere");
        coachA.setYearOfExperience(2);

        Coach coachB = new Coach();
        coachB.setFirstName("Martin");
        coachB.setLastName("Timber");
        coachB.setYearOfExperience(5);

        Trainee traineeA = new Trainee("Timothy", "Elson");
        traineeA.setCoach(coachA);

        Trainee traineeB = new Trainee("Eric", "Wolfie");
        traineeB.setCoach(coachB);

        /*
            abstract fantory
         */
        Manager newManager = ManagerFactory.manager("John", "Smith");

        Coach newCoachA = CoachFactory.regular("Tim", "Peere");
        Coach newCoachB = CoachFactory.senior("Martin", "Timber");

        Trainee newTraineeA = TraineeFactory.withCoach("Timothy", "Elson", newCoachA);
        Trainee newTraineeB = TraineeFactory.withCoach("Eric", "Wolfie", newCoachB);

    }

}
