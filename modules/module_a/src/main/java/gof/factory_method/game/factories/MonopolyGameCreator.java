package gof.factory_method.game.factories;

import gof.factory_method.game.BoardGame;
import gof.factory_method.game.Game;

public class MonopolyGameCreator implements GameFactory {

    @Override
    public Game create() {
        return new BoardGame("Monopoly", "bussiness", 4);
    }
}
