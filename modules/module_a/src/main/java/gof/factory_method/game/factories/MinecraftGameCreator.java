package gof.factory_method.game.factories;

import gof.factory_method.game.Game;
import gof.factory_method.game.PCGame;

public class MinecraftGameCreator implements GameFactory {
    @Override
    public Game create() {
        return new PCGame("Minecraft", "RPG", 1, 99, true);
    }
}
