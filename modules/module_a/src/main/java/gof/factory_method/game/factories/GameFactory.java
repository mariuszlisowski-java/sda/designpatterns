package gof.factory_method.game.factories;

import gof.factory_method.game.Game;

public interface GameFactory {
    Game create();
}
