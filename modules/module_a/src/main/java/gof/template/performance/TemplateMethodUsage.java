package gof.template.performance;

import gof.template.performance.templates.PerformanceTestTemplate;
import gof.template.performance.templates.RandomListSortingPerformanceTest;
import gof.template.performance.templates.StringBuilderAppendPerformanceTest;

public class TemplateMethodUsage {

    public static void main(String[] args) {
        PerformanceTestTemplate testTemplate;

        testTemplate = new RandomListSortingPerformanceTest();
        testTemplate.run();

        testTemplate = new StringBuilderAppendPerformanceTest();
        testTemplate.run();
    }

}
