package gof.interpreter.operations;

import gof.interpreter.operations.interpreters.Interpreter;
import gof.interpreter.operations.interpreters.PythonStyleWithoutOrderMathOperationsInterpreter;
import gof.interpreter.operations.interpreters.WordsWithoutOrderMathOperationsInterpreter;

public class InterpreterUsage {

    public static void main(String[] args) {
        final String MATH_OPERATORS = "2 + 3 * 2";              // works as (2 + 3) * 2
        final String WORD_OPERATORS = "2 ADD 3 MULTIPLY 2";

        final MathOperationApplier mathOperationApplier = new MathOperationApplier();
        Interpreter interpreter = new PythonStyleWithoutOrderMathOperationsInterpreter(mathOperationApplier);

        // FAULTY INTERPRETERS: order of operators!
        String result = interpreter.interpret(MATH_OPERATORS);
        System.out.println(result);

        interpreter = new WordsWithoutOrderMathOperationsInterpreter(mathOperationApplier);
        result = interpreter.interpret(WORD_OPERATORS);
        System.out.println(result);
    }

}
