package gof.interpreter.operations.interpreters.iterator.parking;

import java.util.Iterator;

public class ParkingLotUsage {
    
    public static void main(String[] args) {
        final int CARS_PARKED = 10;
        final ParkingLot parkingLot = new ParkingLot();

        for (int idx = 0; idx < CARS_PARKED; idx++) {
            parkingLot.add(new SimpleCar());
        }

        // custom iterator
        final Iterator<Car> iterator = parkingLot.iterator();
        while (iterator.hasNext()) {
            final Car car = iterator.next();
            System.out.println(car);
        }
    }
    
}
