package gof.interpreter.operations.interpreters;

public interface Interpreter {
    String interpret(String context);
}
