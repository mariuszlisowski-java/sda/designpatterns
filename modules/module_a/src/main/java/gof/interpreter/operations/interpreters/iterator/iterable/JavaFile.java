package gof.interpreter.operations.interpreters.iterator.iterable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class JavaFile implements Iterable<String> {
    private String fileName;
    private String className;
    private List<String> linesContent = new ArrayList<>();

    public void addLine(final String line) {
        linesContent.add(line);
    }

    @Override
    public String toString() {
        return "JavaFile{" +
                "fileName='" + fileName + '\'' +
                ", className='" + className + '\'' +
                ", linesContent=" + String.join("\n", linesContent) +
                '}';
    }

    @Override
    public Iterator<String> iterator() {
        return linesContent.iterator();
    }
}
