package gof.interpreter.operations.interpreters.iterator.iterable;

import java.util.Iterator;

public class Main {

    public static void main(String[] args) {
        JavaFile file = new JavaFile();

        file.addLine("1st line");
        file.addLine("2nd line");
        file.addLine("3rd line");

        Iterator<String> iterator = file.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

    }

}
