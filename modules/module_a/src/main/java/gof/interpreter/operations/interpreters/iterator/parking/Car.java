package gof.interpreter.operations.interpreters.iterator.parking;

public interface Car {
    String getVehicleInfo();
}
