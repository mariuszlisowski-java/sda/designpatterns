package gof.interpreter.operations;

public enum MathOperation {
    ADD,
    SUBTRACT,
    MULTIPLY,
    DIVIDE,
    EXPONENTIATION
}
