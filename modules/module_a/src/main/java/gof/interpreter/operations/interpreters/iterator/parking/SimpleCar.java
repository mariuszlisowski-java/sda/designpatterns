package gof.interpreter.operations.interpreters.iterator.parking;

import lombok.ToString;

@ToString
public class SimpleCar implements Car {
    private static int index = 0;
    private final String info;

    public SimpleCar() {
        info = "Car parked on a lot nubmer " + ++index;
    }

    @Override
    public String getVehicleInfo() {
        return info;
    }
}
