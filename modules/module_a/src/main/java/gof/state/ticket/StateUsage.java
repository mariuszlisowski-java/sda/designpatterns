package gof.state.ticket;

import gof.state.ticket.states.PaidState;
import gof.state.ticket.states.ParkingTicketVendingMachineState;
import gof.state.ticket.states.StillNeedToPayState;

public class StateUsage {

    public static void main(String[] args) {
        final ParkingTicketVendingMachine machine = new ParkingTicketVendingMachine();
        ParkingTicketVendingMachineState state;

        state = new StillNeedToPayState(machine);
        state.openMachineAndAddPrintingPaperPieces();
        state.pressPrintingButton();
        state.moveCreditCardToSensor();

        state = new PaidState(machine);
        state.moveCreditCardToSensor();
        state.openMachineAndAddPrintingPaperPieces();
        state.pressPrintingButton();
    }

}
