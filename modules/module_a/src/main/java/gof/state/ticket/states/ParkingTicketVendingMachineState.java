package gof.state.ticket.states;

public interface ParkingTicketVendingMachineState {
    void moveCreditCardToSensor();
    void pressPrintingButton();
    void openMachineAndAddPrintingPaperPieces();
}
