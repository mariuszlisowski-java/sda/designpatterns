package gof.mediator.graphics.components;

public interface Component {
    void sendRequest();

    default void sendRequest(String context) {
        sendRequest();
    }
}
