package gof.mediator.graphics.mediator;

public interface Mediator {
    void sendInfo(Object requester, String context);
}
