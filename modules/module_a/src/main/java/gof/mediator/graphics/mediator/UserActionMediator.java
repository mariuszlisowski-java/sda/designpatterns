package gof.mediator.graphics.mediator;

import gof.mediator.graphics.components.ActionAppliedMessage;
import gof.mediator.graphics.components.SelectOptions;
import gof.mediator.graphics.components.WarningMessage;

public class UserActionMediator implements Mediator {
    private final ActionAppliedMessage actionAppliedMessage;
    private final SelectOptions selectOptions;
    private final WarningMessage warningMessage;

    public UserActionMediator(final ActionAppliedMessage actionAppliedMessage, final SelectOptions selectOptions, final WarningMessage warningMessage) {
        this.actionAppliedMessage = actionAppliedMessage;
        this.selectOptions = selectOptions;
        this.warningMessage = warningMessage;
        actionAppliedMessage.setMediator(this);
        selectOptions.setMediator(this);
        warningMessage.setMediator(this);
    }

    @Override
    public void sendInfo(Object requester, String context) {
        if (requester == actionAppliedMessage) {
            actionAppliedMessage.displayInfo();
            warningMessage.hideWarning();
            selectOptions.hideOptions();
        } else if (requester == selectOptions) {
            switch (context) {
                case "load":
                    selectOptions.chooseLoad();
                    actionAppliedMessage.displayInfo();
                    break;
                case "restart":
                    selectOptions.chooseRestart();
                    warningMessage.showWarningMessage();
                    break;
                case "save":
                    selectOptions.chooseSave();
                    actionAppliedMessage.displayInfo();
                    break;
            }
        } else if (requester == warningMessage) {
            if (context.equals("hide")) {
                warningMessage.hideWarning();
            } else {
                warningMessage.showWarningMessage();
            }
        }
    }
}
