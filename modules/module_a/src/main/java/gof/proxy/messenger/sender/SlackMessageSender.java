package gof.proxy.messenger.sender;

import gof.proxy.messenger.Message;

import java.util.ArrayList;
import java.util.List;

public class SlackMessageSender implements MessageSender {
    private final List<Message> messages = new ArrayList<>();

    @Override
    public void sendMessage(String channelName, String username, String messageText) {
        final Message message = new Message(channelName, username, messageText);
        System.out.println("Sent message " + message);
        messages.add(message);
        System.out.println("Messages: " + messages);
    }
}
