package gof.proxy.messenger;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Message {
    private String channelName;
    private LocalDateTime postDate;
    private String author;
    private String text;

    public Message(String channelName, String author, String text) {
        this.channelName = channelName;
        this.author = author;
        this.text = text;
        this.postDate = LocalDateTime.now();
    }
}
