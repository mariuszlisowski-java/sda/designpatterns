package gof.proxy.messenger.sender;

public interface MessageSender {
    void sendMessage(String channelName, String username, String message);
}
